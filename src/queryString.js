const stringifyPrimitive = v => {
    switch (typeof v) {
      case 'string':
        return v
  
      case 'boolean':
        return v ? 'true' : 'false'
  
      case 'number':
        return isFinite(v) ? v : ''
  
      default:
        return ''
    }
}

export default (data, headers) => {
    if (typeof data === 'object') {
        return Object.keys(data).map(k => {
            const ks = encodeURIComponent(stringifyPrimitive(k)) + '='
            if (Array.isArray(data[k])) {
                return data[k].map(function(v) {
                return ks + encodeURIComponent(stringifyPrimitive(v))
                }).join('&')
            } else {
                return ks + encodeURIComponent(stringifyPrimitive(data[k]))
            }
        }).filter(Boolean).join('&')
    
    }

    return ''
}