import vote from "./vote";

const USERNAME = process.argv[2]
const TIMEOUT = 10 * 60 * 1000
const DO_LOOP = !process.argv.includes('--no-loop')

if (!USERNAME) {
    console.error("Please call the CLI with a username as argument")
    process.exit()
} else {
    console.log('Running Mineweb auto-vote v1.0');
    if (DO_LOOP) console.log(`The bot will vote for ${USERNAME} every ${TIMEOUT / 60000} minutes.`)
    else console.log(`The bot will vote for ${USERNAME}.`)
}

const voteSession = () => {
    new Promise(a => a())
    .then(() => console.log("Started voting session"))

    //GAMMA
    .then(() => vote(USERNAME, 7)) // MINECRAFT-TOP.COM
    .then(() => vote(USERNAME, 8)) // SERVEUR-PRIVE.NET
    .then(() => vote(USERNAME, 9)) // Rpg-Paradize
}

const main = () => {
    voteSession()
    setTimeout(main, TIMEOUT) // every ten minutes
}

if (DO_LOOP) main()
else voteSession()
