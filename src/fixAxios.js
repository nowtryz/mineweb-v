import axios from "axios"
import axiosCookieJarSupport from 'axios-cookiejar-support'
import { CookieJar } from 'tough-cookie'
import queryString from "./queryString";


const instance = axios.create({
    baseURL: 'https://ycraft.fr/',
    withCredentials: true,
    transformRequest: [queryString],
    headers: {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Referer': 'https://ycraft.fr/vote',
        'Origin': 'https://ycraft.fr',
        'X-Requested-With': 'XMLHttpRequest' // Bypass MineWeb's `$this->request->is('ajax')`
    }
})

axiosCookieJarSupport(instance)
instance.defaults.jar = new CookieJar()

export default instance