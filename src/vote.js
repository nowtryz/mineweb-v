import axios from './fixAxios'


const getCRCHToken = async() => {
    const response = await axios.get('/', {
        responseType: 'document'
    })

    const match = response.data.match(/var CSRF_TOKEN = "(?<token>[A-Za-z0-9]+)";/)
    return match.groups.token
}


const login = (csrfToken, username) => axios.post('/vote/step/user', {
    'data[_Token][key]': csrfToken,
    'username': username
})


const website = (csrfToken, id) => axios.post('/vote/step/website', {
    'data[_Token][key]': csrfToken,
    'website_id': id
})


const check = async token => {
    const response = axios.get('/vote/step/check')
    // true on succes if possiuble
    return response
}


const reward = csrfToken => axios.post('/vote/step/reward', {
    'data[_Token][key]': csrfToken,
    'reward_time': 'LATER'
})


export default async (username, siteId) => {
    const token = await getCRCHToken()
    await login(token, username)
    .then(({data}) => console.log(`[${siteId}] ${data.msg}`))
    .then(() => website(token, siteId))
    .then(({data}) => {
        if (!data.status) throw data.error
        else console.log(`[${siteId}] ${data.success}`)
    })
    .then(() => check(token))
    .then(({data}) => {
        if (!data.status) throw data.error
        else if (!data.reward_later) throw 'cannot reward later'
        else console.log(`[${siteId}] ${data.success}`)
    })
    .then(() => reward(token))
    .then (({data}) => {
        if (data.status) return `[${siteId}] ${data.success}`
        else return `[${siteId}] ${data.error}`
    })
    .then(console.log)
    .catch(err => console.error(`[${siteId}]`, err))
}