Auto voter
==========

## How to use

### Requirements
 - [Node.js](https://nodejs.org/)
 - [NPM](https://www.npmjs.com/) or [yarn](https://yarnpkg.com/)

### Install and run
 - Install:
   ````shell
   # NPM
   npm install
   # Yarn
   yarn
   ```
 - Run:
   ```shell
   # NPM
   npm start <minecraft username>
   # Yarn
   yarn start <minecraft username>
   ```